package com.vz.security.config.settings


case class configmetadata (configtype: String, module: String, description: String)
case class configparentconfig(appname: String, id: String, timestamp: Long, meta: configmetadata,  vid: String)
