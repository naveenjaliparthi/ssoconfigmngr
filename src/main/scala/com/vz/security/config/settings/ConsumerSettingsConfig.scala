package com.vz.security.config.settings

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import akka.kafka.{AutoSubscription, ConsumerSettings, Subscriptions}
import akka.stream.Supervision
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties}
import com.typesafe.config.{Config, ConfigFactory}

import scala.collection.JavaConverters._

object ConsumerSettingsConfig {


  var config: Config = ConfigFactory.load()
  val kafkaConfigList: List[Config] = config.getConfigList("config_manager.kafka").asScala.toList

  def getCosumerSettingsKafka(): ConsumerSettings[String, String] = {
    val consumerConfig: Config = config.getConfig("config_manager.kafkaConsumer")
    val kafkaInst = config.getString("config_manager.consumer.kafka.instances")
    val consumerSettings: ConsumerSettings[String, String] = getConsumerSettings(consumerConfig, kafkaInst)
    consumerSettings
  }

  def getConsumerSettings(consumerConfig: Config, kafkaName: String): ConsumerSettings[String, String] = {
    val kc: Option[Config] = kafkaConfigList collectFirst {
      case c if c.getString("name").equalsIgnoreCase(kafkaName) => c
    }
    if(kc.isEmpty) None

    val kafkaConfig = kc.get

    val kafkaType = kafkaConfig.getString("security.kafkatype")
    val consumerSettings: ConsumerSettings[String, String] = kafkaType match {
      case "JAAS" => getJaasSettings(consumerConfig, kafkaConfig)
      case "SSL" => getSSLSettings(consumerConfig, kafkaConfig)
      case "PLAIN" => getPlainSettings(consumerConfig, kafkaConfig)
    }
    consumerSettings
  }

  def getPlainSettings(consumerConfig: Config, kafkaConfig: Config) : ConsumerSettings[String, String] = {
    val consumerSettings: ConsumerSettings[String, String] =
      ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(kafkaConfig.getString("url"))
        .withGroupId(config.getString("config_manager.app.groupId")) //as groupId
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
        .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
        .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    consumerSettings
  }

  def getJaasSettings(consumerConfig: Config, kafkaConfig: Config) : ConsumerSettings[String, String] = {
    val consumerSettings: ConsumerSettings[String, String] =
      ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(kafkaConfig.getString("url"))
        .withGroupId(config.getString("config_manager.app.groupId")) //as groupId
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
        .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
        .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty("security.protocol", "SASL_PLAINTEXT")
        .withProperty("sasl.mechanism", "PLAIN")
        .withProperty("sasl.jaas.config",kafkaConfig.getString("security.jaas.jaasConfig"))
    consumerSettings
  }



  def getSSLSettings(consumerConfig: Config, kafkaConfig: Config) : ConsumerSettings[String, String] = {
    val consumerSettings: ConsumerSettings[String, String] =
      ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(kafkaConfig.getString("url"))
        .withGroupId(config.getString("config_manager.app.groupId")) //as groupId
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
        .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
        .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty("security.protocol" , "SSL")
        .withProperty("ssl.truststore.location" , kafkaConfig.getString("security.ssl.truststore.location"))
        .withProperty("ssl.truststore.password" , kafkaConfig.getString("security.ssl.truststore.password"))
        .withProperty("ssl.keystore.location" , kafkaConfig.getString("security.ssl.keystore.location"))
        .withProperty("ssl.keystore.password" , kafkaConfig.getString("security.ssl.keystore.password"))
        .withProperty("ssl.key.password" , kafkaConfig.getString("security.ssl.key.password"))
        .withProperty("ssl.truststore.type" , "JKS")
        .withProperty("ssl.keystore.type" , "JKS")
        .withProperty("ssl.enabled.protocols" , "TLSv1.2")
    consumerSettings
  }

  def getElasticSettings(esClusterConf: String) =  {
    val esClient = ElasticClient(ElasticProperties(esClusterConf))
    esClient
  }

  lazy val flowDecider: Supervision.Decider = {
    case ex: Exception =>
      Supervision.Resume
  }

}
