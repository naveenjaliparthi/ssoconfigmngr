package com.vz.security.config

import java.util.UUID
import java.util.concurrent.Executors

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.kafka.{AutoSubscription, ConsumerSettings, Subscriptions}
import akka.kafka.scaladsl.Consumer
import akka.stream.{ActorAttributes, ActorMaterializer, ActorMaterializerSettings, ClosedShape}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import com.sksamuel.elastic4s.http.search.SearchResponse
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.config.settings.ConsumerSettingsConfig
import com.vz.security.sso.logging.VZLogger
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jackson.{JsonMethods => json4s}

import scala.collection.JavaConverters._
import scala.collection.JavaConverters._
import scala.collection.immutable.ListMap
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.collection.mutable.{HashMap, ListBuffer}
import com.sksamuel.elastic4s.http.Response
import com.vz.security.config.services.ConfigMgrElasticWriter

import scala.collection.mutable


trait ConfigManagerClient {
  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats
  val config: Config = ConfigFactory.load()

  def initApp(appList: List[String]): Future[String]

  def getAppConfiguration(appname: String): Config

  def getStringField(appname: String, configkey: String): Option[String]

  def getMapField(appname: String, configkey: String): Option[Map[String, AnyRef]]
}

class ConfigurationManagerLib(actorSystem: Option[ActorSystem]) extends ConfigManagerClient {

  implicit val providedActorSystem: ActorSystem = actorSystem.getOrElse(ActorSystem("SecurityConfigManager", config))
  implicit val mat: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(providedActorSystem).withSupervisionStrategy(ConsumerSettingsConfig.flowDecider))

  val configCacheMap: HashMap[String, Config] = HashMap.empty[String, Config]
  val esClusterConf: String = config.getString("config_manager.elastic.sso_elastic_cluster")

  val esClient = ConsumerSettingsConfig.getElasticSettings(esClusterConf)

  override def getAppConfiguration(appname: String): Config = {
    try {
      configCacheMap.get(appname).get
    } catch {
      case ex: Exception =>
        null
    }
  }

  override def getStringField(appname: String, configkey: String): Option[String] = {
    try {
      Option(getAppConfiguration(appname).getValue(configkey).unwrapped().toString)
    } catch {
      case ex: Exception =>
       None
    }
  }

  //Works only if value is Map
  override def getMapField(appname: String, configkey: String): Option[Map[String, AnyRef]] = {
    try {
      Option(getAppConfiguration(appname).getObject(configkey).unwrapped().asScala.toMap)
    } catch {
      case ex: Exception =>
        None
    }
  }

  override def initApp(appList: List[String]): Future[String] = {
    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4))
    val uuid: String = java.util.UUID.randomUUID().toString
    val noSink: Sink[Any, Future[Done]] = Sink.ignore
    //val configLoaderKafkaTopics: Set[String] = config.getStringList("config_manager.consumer.kafka.topic").asScala.foldLeft(Set[String]()) { (set, topic) => set + topic }

    val configLoaderKafkaTopics = config.getStringList("config_manager.consumer.kafka.topic").asScala.toList
    val consumerSettings = ConsumerSettingsConfig.getCosumerSettingsKafka().withGroupId(UUID.randomUUID().toString) // Use random ID for in-memory

    val subscription: AutoSubscription =
      Subscriptions.topics(configLoaderKafkaTopics.toSet)

    val plainKafkaSource: Source[ConsumerRecord[String, String], Consumer.Control] =
      Consumer.plainSource(consumerSettings, subscription)
        .withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    val mapFromConsumerRecord: Flow[ConsumerRecord[String, String], String, NotUsed] =
      Flow[ConsumerRecord[String, String]].map(record => record.value())
        .withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    val checkIfValid: Flow[String, String, NotUsed] = Flow[String].filter(jsonString => {
      val vzlogger = new VZLogger(this, "ConfigManager")
      try {
        val json = parse(jsonString)
        val appContext = json.extract[Map[String, Any]].get("appname")

        if (appContext.nonEmpty && appList.contains(appContext.get.toString)) true else false
      } catch {
        case ex: Exception =>
          vzlogger.error(ex)
          false
      }
    }).withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    val updateConfig = Flow[String].map(jsonString => {
      val vzlogger = new VZLogger(this, "UpdateConfigurationManager")
      val appContext = parse(jsonString).extract[Map[String, Any]].get("appname")
      vzlogger.addKeyValue("UpdatingConfig", appContext)
      //configCacheMap += (appContext.get.toString.replaceAll("\"","") -> jsonString.toString)
      configCacheMap += (appContext.get.toString.replaceAll("\"", "") -> ConfigFactory.parseString(jsonString.toString))

      vzlogger.addKeyValue("UpdatingConfig", "configCacheMap " + jsonString.toString + " mapsize: " + configCacheMap.size)
      vzlogger.info
      jsonString
    }).withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    val es_config_index = "configmanager"
    val es_configver_index = "configversion"

    val appstrList = getParentConfig(appList)
    val execInit = for {
      resultParentES <- {
        val resConfig = queryElasticConfig(appstrList.toString, es_config_index, "appname")
        resConfig map {
          result =>
            handleMatchResultParenES(result)
        }
      }
      resultConfigVersionES <- {
        val resConfig = queryElasticConfigVersion(resultParentES.toString, es_configver_index, "vid")
        resConfig map {
          result =>
            handleMachResultConfigVersionES(result)
        }
      }

    } yield (resultConfigVersionES)
    execInit map {
      res =>
        val enableSaveToElastic: Boolean = config.getBoolean("config_manager.elastic.enable_elastic_save")
        if (enableSaveToElastic) new ConfigMgrElasticWriter(actorSystem).initApp // START ELASTIC PERSISTENCE

        lazy val runnableGraph: RunnableGraph[NotUsed] = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder =>
          import GraphDSL.Implicits._
          plainKafkaSource ~> mapFromConsumerRecord ~> checkIfValid ~> updateConfig ~> noSink
          ClosedShape
        })
        runnableGraph.run()
    }
    execInit
  }

  def getParentConfig(appList: List[String]): String = {
    val appstr = appList mkString " OR "
    appstr.toString()
  }

  def handleMatchResultParenES(parentList: Response[SearchResponse]): String = {
    //val hitsP = parentList.hits.hits
    val hitsP = parentList.result.hits.hits
    val parentresList = new ListBuffer[String]()

    hitsP.foreach { allhitsParent =>
      val resulthitsParent = allhitsParent.sourceAsString
      val configParentId = parse(resulthitsParent).extract[Map[String, Any]].get("vid")

      val confparentvalue: String = configParentId match {
        case Some(s: String) => s
        case None => ""
      }
      parentresList += confparentvalue
    }

    parentresList.toList mkString " OR "
  }

  def handleMachResultConfigVersionES(versionList: Response[SearchResponse]): String = {
    val vzlogger = new VZLogger(this, "LoadConfigurationManager")

    val tmpConfigContextCacheMap: HashMap[String, String] = HashMap.empty[String, String]
    //val hits = versionList.hits.hits
    val hits = versionList.result.hits.hits

    hits.foreach { allhits =>
      val resulthits = allhits.sourceAsString
      val appjson = json4s.parse(resulthits)
      val foundappname = json4s.pretty(json4s.render(appjson \ "appname"))
      val foundtimestamp = json4s.pretty(json4s.render(appjson \ "timestamp"))
      tmpConfigContextCacheMap += (foundtimestamp -> (foundappname.replaceAll("\"", "") + "&&" + resulthits))
      val sortedList = ListMap(tmpConfigContextCacheMap.toSeq.sortBy(_._1): _*)
      sortedList.map {
        sdata =>
          val dataArr = sdata._2.split("&&")
          configCacheMap += (dataArr(0).replaceAll("\"", "") -> ConfigFactory.parseString(dataArr(1)))
      }
    }

    if (hits.size > 0) "Done" else null
  }

  def queryElasticConfig(inputStr: String, es_config_index: String, es_match_query: String): Future[Response[SearchResponse]] = {
    import com.sksamuel.elastic4s.http.ElasticDsl._

    val futureResult: Future[Response[SearchResponse]] = esClient.execute {
      search(es_config_index).matchQuery(es_match_query, inputStr.toUpperCase()).size(200)
    }
    futureResult
  }

  def queryElasticConfigVersion(inputStr: String, es_config_index: String, es_match_query: String): Future[Response[SearchResponse]] = {
    import com.sksamuel.elastic4s.http.ElasticDsl._

    val futureResult: Future[Response[SearchResponse]] = esClient.execute {
      search(es_config_index).matchQuery(es_match_query, inputStr).sortByFieldAsc("timestamp").size(1000)
    }
    futureResult

  }

}
