package com.vz.security.config.services

import java.util.concurrent.Executors

import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer
import akka.kafka.{AutoSubscription, Subscriptions}
import akka.stream.scaladsl.{Flow, GraphDSL, RunnableGraph, Sink, Source}
import akka.stream.{ActorAttributes, ActorMaterializer, ActorMaterializerSettings, ClosedShape}
import akka.{Done, NotUsed}
import com.sksamuel.elastic4s.http.index.IndexResponse
import com.sksamuel.elastic4s.http.update.UpdateResponse
import com.sksamuel.elastic4s.RefreshPolicy
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties, Response}
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.config.settings.{ConsumerSettingsConfig, configmetadata, configparentconfig}
import com.vz.security.sso.logging.VZLogger
import org.apache.kafka.clients.consumer.ConsumerRecord

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
/*json serializer*/
import java.util.Calendar

import org.json4s.{DefaultFormats, _}
import org.json4s.jackson.JsonMethods.parse
import org.json4s.jackson.{JsonMethods => json4s}
import org.json4s.native.Serialization.write


class ConfigMgrElasticWriter(actorSystem: Option[ActorSystem]) {

  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats
  val config: Config = ConfigFactory.load()

  implicit val providedActorSystem: ActorSystem = actorSystem.getOrElse(ActorSystem("SecurityConfigManager", config))
  implicit val mat: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(providedActorSystem).withSupervisionStrategy(ConsumerSettingsConfig.flowDecider))
  val esClusterConf: String = config.getString("config_manager.elastic.sso_elastic_cluster")

  //val enableSaveToElastic: String = config.getString("config_manager.elastic.enable_elastic_save")
  val esClient = ConsumerSettingsConfig.getElasticSettings(esClusterConf)

  def initApp = {
    import com.sksamuel.elastic4s.http.ElasticDsl._
    implicit val executionContext: ExecutionContextExecutor = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(4))
    val uuid: String = java.util.UUID.randomUUID().toString
    val noSink: Sink[Any, Future[Done]] = Sink.ignore
    //val configLoaderKafkaTopics: Set[String] = config.getStringList("config_manager.consumer.kafka.topic").asScala.foldLeft(Set[String]()) { (set, topic) => set + topic }
    val configLoaderKafkaTopics = config.getStringList("config_manager.consumer.kafka.topic").asScala.toList
    val consumerSettings = ConsumerSettingsConfig.getCosumerSettingsKafka()

    val subscription: AutoSubscription =
      //Subscriptions.topics(configLoaderKafkaTopics)
      Subscriptions.topics(configLoaderKafkaTopics.toSet)

    val plainKafkaSource: Source[ConsumerRecord[String, String], Consumer.Control] =
      Consumer.plainSource(consumerSettings, subscription)
        .withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    val mapFromConsumerRecord: Flow[ConsumerRecord[String, String], String, NotUsed] =
      Flow[ConsumerRecord[String, String]].map(record => record.value()  )
        .withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    val checkIfValid: Flow[String, String, NotUsed] = Flow[String].filter(jsonString => {
      val vzlogger = new VZLogger(this, "ConfigManagerService")
      try {
        val json = parse(jsonString)
        val appContext = json.extract[Map[String, Any]].get("appname")
        vzlogger.addKeyValue("configname", appContext)
        vzlogger.addKeyValue("nonEmpty", appContext.nonEmpty)
        vzlogger.info
        appContext.nonEmpty
      } catch {
        case ex: Exception =>
          vzlogger.addKeyValue("checkIfValid", ex.printStackTrace())
          vzlogger.info
          false
      }
    })
      .withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    //Update the elastic entries for parent and version based on UUID
    val updateConfig = Flow[String].map(jsonString => {
      val vzlogger = new VZLogger(this, "ConfigurationManager")
      val enableSaveToElastic: Boolean = config.getBoolean("config_manager.elastic.enable_elastic_save")

      vzlogger.addKeyValue("enableSaveToElastic", enableSaveToElastic)

      try {
        if (enableSaveToElastic) {
          val appContext = parse(jsonString).extract[Map[String, Any]].get("appname")
          val today = Calendar.getInstance() getTimeInMillis
          val configjson = json4s.parse(jsonString.toString)
          val _appname = json4s.pretty(json4s.render(configjson \ "appname"))
          val _id = json4s.pretty(json4s.render(configjson \ "id"))
          val _meta = json4s.pretty(json4s.render(configjson \ "meta"))
          val _vid = json4s.pretty(json4s.render(configjson \ "vid"))
          val _metajson = json4s.parse(_meta.toString)
          val _configtype = json4s.pretty(json4s.render(_metajson \ "configtype"))
          val _module = json4s.pretty(json4s.render(_metajson \ "module"))
          val _description = json4s.pretty(json4s.render(_metajson \ "description"))

          vzlogger.addKeyValue("SaveToElastic", _id.toString)
          val toElasticUpdateParent = configparentconfig(_appname.replaceAll("\"", ""), _id.replaceAll("\"", ""), today, configmetadata(_configtype, _module, _description), _vid)
          val futureResult: Future[Response[UpdateResponse]] = esClient.execute {
            update(_id.toString).in("configmanager/config").docAsUpsert(write(toElasticUpdateParent).toString).refresh (RefreshPolicy.Immediate)
          }
          val childResult: Future[Response[IndexResponse]] = esClient.execute {
            indexInto("configversion" / "config").doc(jsonString.toString).refresh(RefreshPolicy.Immediate)
          }


          vzlogger.addKeyValue("Id", _id)
          vzlogger.addKeyValue("Value", jsonString.toString)
          vzlogger.info
        }
      } catch {
        case ex: Exception =>
          vzlogger.addKeyValue("updateConfigException", ex.printStackTrace())
          vzlogger.info
      }

    }).withAttributes(ActorAttributes.supervisionStrategy(ConsumerSettingsConfig.flowDecider))

    lazy val runnableGraph: RunnableGraph[NotUsed] = RunnableGraph.fromGraph(GraphDSL.create() { implicit builder =>
      import GraphDSL.Implicits._
      plainKafkaSource ~> mapFromConsumerRecord ~>  checkIfValid ~> updateConfig ~> noSink
      ClosedShape
    })
    runnableGraph.run()
  }
}

