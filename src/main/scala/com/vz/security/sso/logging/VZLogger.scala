package com.vz.security.sso.logging

import java.util.regex.Pattern

import org.json4s._
import org.json4s.jackson.Serialization.write
import org.slf4j.{LoggerFactory, MarkerFactory}

trait VZLogging {

  protected val logger: VZLogger = new VZLogger(getClass.getName)

}


class VZLogger(className: Any, typeAlias: String = "logmessage", sessionId: String = "sessionIdNotProvided") {

  private var loggerMap = Map[String, Any](
    "type_alias" -> typeAlias,
    "className" -> className.getClass.getSimpleName,
    "startTime" -> System.currentTimeMillis(),
    "session_id" -> sessionId
  )

  private val logger = LoggerFactory.getLogger(className.getClass)
  private val loggerIdentifierText = "LogThis"
  private val logThisMarker = MarkerFactory.getMarker(loggerIdentifierText)
  val pattern = Pattern.compile("[^a-zA-Z0-9_]+")

  implicit val formats = DefaultFormats

  // function to add more information to custom log
  def addKeyValue(key: String, value: Any): Unit = {
    if (key.nonEmpty) {
      val mkey = pattern.matcher(key).replaceAll("").toLowerCase
      loggerMap += (mkey ->value)
    }
  }

  def info: Unit =
    logger.info(logThisMarker, generateLog)

  def debug: Unit =
    logger.debug(logThisMarker, generateLog)

  def error(exception: Throwable): Unit = {
    setError(exception)
    logger.error(logThisMarker, generateLog)
  }

  def setError(exception: Throwable): Unit = {
    loggerMap += (("errorName", exception.getMessage))
    loggerMap += (("stackTrace", exception.getStackTrace.mkString("\r\n\t")))
  }

  private def generateLog: String = {
    if (loggerMap.nonEmpty) {
      loggerMap += ("endTime" -> System.currentTimeMillis())
      val jsonStr = write(loggerMap)
      return jsonStr.substring(1, jsonStr.length - 1)
    }
    ""
  }

}