package com.vz.security.sso.configmanager.kafka

import java.util.{Properties}
import com.vz.security.sso.logging.VZLogger
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.collection.JavaConverters._
import scala.concurrent.Future
object KafkaPublisher {

  val conf: Config = ConfigFactory.load()
  val kafkaConfigList: List[Config] = conf.getConfigList("config_manager.kafka").asScala.toList
  val confMngrConf: Config = conf.getConfig("config_manager.publisher.kafka")
  val topics = confMngrConf.getStringList("topic")
  val topicsList:  List[String] = topics.asScala.map { elem => {
    elem
  }}.toList
  val topicWithKey = confMngrConf.getBoolean("topicWithKey")
  val kafkaInstances = confMngrConf.getStringList("instances")
  val kafkaList:  List[String] = kafkaInstances.asScala.map { elem => {
    elem
  }}.toList

  def getKafkaInstanceNames: List[String] =  kafkaConfigList.map { x => {
    x.getString("name")
  }}

  def getKafkaProperties(kafkaName: String, hasKey: Boolean): Option[Properties] = {
    val kc: Option[Config] = kafkaConfigList collectFirst {
      case c if c.getString("name").equalsIgnoreCase(kafkaName) => c
    }
    if(kc.isEmpty) None

    val kafkaConfig = kc.get
    val props = new Properties()
    props.put("name", kafkaConfig.getString("name"))
    props.put("bootstrap.servers", kafkaConfig.getString("url"))
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer ")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    if (kafkaConfig.getString("security.kafkatype").equalsIgnoreCase("JAAS")) {
      //Todo: check here if its kafka config
      addJaasProps(props, kafkaConfig)
    }
    else if (kafkaConfig.getString("security.kafkatype").equalsIgnoreCase("SSL")) {
      addSSLProperties(props, kafkaConfig)
    } else { // Plain
      Some(props)
    }
  }

  //Adding JAAS properties
  def addJaasProps(props: Properties, conf: Config): Option[Properties] = {
    props.put("security.protocol", "SASL_PLAINTEXT")
    props.put("sasl.mechanism", "PLAIN")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("sasl.jaas.config", conf.getString("security.jaas.jaasConfig"))
    //ToDo: Check for optional value
    Some(props)
  }

  //Adding SSL properties
  def addSSLProperties(props: Properties, conf: Config): Option[Properties] = {
    props.put("security.protocol" , "SSL")
    props.put("ssl.truststore.location" , conf.getString("security.ssl.truststore.location"))
    props.put("ssl.truststore.password" , conf.getString("security.ssl.truststore.password"))
    props.put("ssl.keystore.location" , conf.getString("security.ssl.keystore.location"))
    props.put("ssl.keystore.password" , conf.getString("security.ssl.keystore.password"))
    props.put("ssl.key.password" , conf.getString("security.ssl.key.password"))
    props.put("ssl.truststore.type" , "JKS")
    props.put("ssl.keystore.type" , "JKS")
    props.put("ssl.enabled.protocols" , "TLSv1.2")
    //ToDo: Check for optional value
    Some(props)
  }

  def publishParentConfigToKafka(id: String, confMngrOp: Option[String]) : Future[Boolean] = {
    val req = confMngrOp.getOrElse(throw new NullPointerException("kafka request missing"))

    val futureOps = kafkaList.map { kafkaName =>
      val props: Option[Properties] = getKafkaProperties(kafkaName, topicWithKey)
      publishToKafka(req, props, id)
    }
    import scala.concurrent.ExecutionContext.Implicits.{ global => ImplicitsGlobal }
    Future.foldLeft(futureOps)(false){ case(finalRes, res) =>
      finalRes || res
    }
  }

  def publishToKafka(confMngrOp: String, props: Option[Properties], id: String): Future[Boolean] = {
    val vzlogger = new VZLogger(this, "config_kafka_publish")
    vzlogger.addKeyValue("configId", id)
    val producer: KafkaProducer[String, String] = new KafkaProducer[String, String](props.get)
    val futureOpList = topicsList.map { topic =>
      publishMsgToTopic(confMngrOp, topic, topicWithKey, id, producer)
    }
    import scala.concurrent.ExecutionContext.Implicits.{global => ImplicitsGlobal}
    Future.foldLeft(futureOpList)(false) { case (finalRes, res) =>
      finalRes || res
    }
  }

  def publishMsgToTopic(msg: String, topic: String, hasKey: Boolean, key: String, producer: KafkaProducer[String, String]): Future[Boolean] = {
    try {
      val pr: ProducerRecord[String, String] = if(hasKey){
        new ProducerRecord(topic, key , msg)
      } else {
        new ProducerRecord(topic, null, msg)
      }
      producer.send(pr)
      Future.successful(true)
    } catch {
      case e: Exception =>
        Future.failed(e)
    }
  }
}
