package com.vz.security.sso.models

case class ConfigMeta(configtype: String, module: String, description: String)
case class ConfigOb(id: String, appname: String, meta: ConfigMeta)