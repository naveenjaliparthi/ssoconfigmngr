package com.vz.security.config.filter

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.vz.security.config.ConfigurationManagerLib
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.Success
//import pl.allegro.tech.embeddedelasticsearch.{EmbeddedElastic, PopularProperties}

import scala.concurrent.ExecutionContextExecutor

class ConfigLibModuleTest extends TestKit(ActorSystem("ConfigManager")) with Matchers with WordSpecLike  with ScalaFutures with BeforeAndAfterAll {
  //implicit lazy val system: ActorSystem = ActorSystem("AkkaKafkaSystem")

  implicit lazy val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  val configclient = new ConfigurationManagerLib(Option(null))

  override def beforeAll(): Unit = {
    val conf = configclient.initApp(List("DynamicDev","VZPEDROTESTAPPV7"))
    Await.result(conf, 3000 millis)
  }


  "The config lib init - DynamicDev" should {
    "return value" in {
      assert(configclient.getStringField("DynamicDev", "configvalue.config.ssoakkaservices.destKey").getOrElse("Pedro") == "destKey456")
    }
  }

  "The config lib init " should {
    "return value" in {
      assert(!configclient.getAppConfiguration("DynamicDev").isEmpty)
    }
  }

  "The config lib init " should {
    "return value in getting config" in {
      try {
        val test = configclient.getStringField("DynamicDev", "configvalue.config.xmdnDistinctCount2").getOrElse("Pedro")
       // println(test)
      } catch {
        case ex: Exception =>
        val test = "Test from Except"
          println(test)
      }

      assert(!configclient.getStringField("DynamicDev", "configvalue.config.xmdnDistinctCount").isEmpty)
    }
  }
}


/*
     "return value" in {

       whenReady(configclient.getAppContextField("DynamicDev", "configvalue.config.xmdnDistinctCount").getOrElse("").toString) {
         configvalue => {
           configvalue.length > 0 shouldBe true
         }
       }

     }*/

    /*
        whenReady(configclient.getAppConfiguration("DynamicDev")) {
          configvalue => {
            println("First Test:: " +configvalue)
          }
        }
        whenReady(configclient.getAppContextField("DynamicDev","configvalue.config.xmdnDistinctCount").getOrElse("")) {
          configvalue => {
            println(configvalue)
          }
        }

      */
