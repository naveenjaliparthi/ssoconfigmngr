import sun.security.tools.PathList

name := "ssoconfigmngr"

organization := "com.verizon"

version := "4.3.0"


scalaVersion := "2.12.4"

val akkaVersion = "2.5.16"
//val elasticVersion = "5.5.3"
val elasticVersion = "6.3.3"
val jacksonVersion = "2.9.8"
val json4sVersion = "3.6.4"


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "org.json4s" %% "json4s-native" % json4sVersion,
  "org.json4s" %% "json4s-jackson" % json4sVersion,
  "com.typesafe.akka" %% "akka-stream-kafka" % "0.18",
  "org.apache.kafka" % "kafka-clients" % "1.0.0",
  "com.thesamet.scalapb" %% "scalapb-runtime" % "0.7.1",
  "commons-io" % "commons-io" % "2.6",
  "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.sksamuel.elastic4s" %% "elastic4s-http" % elasticVersion,
  "com.github.danielwegener" % "logback-kafka-appender" % "0.2.0-RC1",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.8" % Test
  //"com.sksamuel.elastic4s" %% "elastic4s-testkit" % elasticVersion % "test",
  //"com.sksamuel.elastic4s" %% "elastic4s-embedded" % elasticVersion % "test"
)
